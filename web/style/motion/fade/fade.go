package fade

import (
	"github.com/gernest/gs"
	"github.com/gernest/vected/web/mixins"
	"github.com/gernest/vected/web/themes"
)

const (
	FadeIn    = "fadeIn"
	FadeOut   = "fadeOut"
	FrameName = "fade"
)

func Fade(klass, keyframe string) gs.CSSRule {
	return gs.CSS(
		mixins.MakeMotion(klass, keyframe, themes.Default.AnimationDurationBase),
		gs.S(klass+"-enter",
			gs.S("&,"+klass+"-appear",
				gs.P("opacity", "0"),
				gs.P("animation-timing-function", "linear"),
			),
		),
		gs.S(klass+"-leave",
			gs.P(" animation-timing-function", "linear"),
		),
	)
}

func KeyFrame() gs.CSSRule {
	return gs.CSS(
		gs.KeyFrame(FadeIn,
			gs.Cond("0%",
				gs.P("opacity", "0"),
			),
			gs.Cond("100%",
				gs.P("opacity", "1"),
			),
		),
		gs.KeyFrame(FadeOut,
			gs.Cond("0%",
				gs.P("opacity", "1"),
			),
			gs.Cond("100%",
				gs.P("opacity", "0"),
			),
		),
	)
}
